const { ADMIN_EMAIL, ADMIN_PASSWORD } = require("../src/config");
const { hashedPassword } = require("../src/helpers");

const UserSeeder = {
  name: "Super Admin",
  email: ADMIN_EMAIL,
  password: hashedPassword(ADMIN_PASSWORD),
  phoneNumber: "09789506439",
  userTypes: "super_admin",
};

const EmployeeSeeder = {
  employeeId: "MySol-Super-Employee",
  employee: "MySol Super Employee",
  email: "mysol@gmail.com",
  password: hashedPassword(ADMIN_PASSWORD),
  phoneNumber: "09773792671",
  address: "Naing Group Sule Tower (2), Kyauktada Township, Yangon, Myanmar",
  position: "super-Employee",
  employeeType: "Permanent",
  department: "BOD",
};

module.exports = { UserSeeder, EmployeeSeeder };
