const { PrismaClient } = require("@prisma/client");
const { UserSeeder, EmployeeSeeder } = require("./databaseSeeder");
const prisma = new PrismaClient();

const main = async () => {
  await prisma.user.create({
    data: UserSeeder,
  });
  await prisma.employee.create({
    data: EmployeeSeeder,
  });
};

main()
  .catch((error) => {
    throw error;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
