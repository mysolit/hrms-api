import { Router } from "express";
import { body } from "express-validator";
import { UserSignin } from "../controllers/UserController";

export const UserRouter = Router();

UserRouter.post(
  "/userSignin",
  [body("email").notEmpty(), body("password").notEmpty()],
  UserSignin
);
