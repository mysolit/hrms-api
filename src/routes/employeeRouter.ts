import { Router } from "express";
import { body } from "express-validator";
import {
  CreateEmployeeInfo,
  EmployeeLists,
  Signin,
  SingleEmployee,
} from "../controllers/EmployeeController";
//
export const EmployeeRouter = Router();

EmployeeRouter.post(
  "/signin",
  [body("email").notEmpty(), body("password").notEmpty()],
  Signin
);
EmployeeRouter.post(
  "/createEmployeeInfo",
  [
    body("employeeId").notEmpty(),
    body("employee").notEmpty(),
    body("email").notEmpty(),
  ],
  CreateEmployeeInfo
);

EmployeeRouter.get("/employeeLists", EmployeeLists);

EmployeeRouter.get("/:id", SingleEmployee);
