import { compareSync, hashSync } from "bcrypt";
import { sign } from "jsonwebtoken";
import { JWT_SECRET } from "../config";
import { EmployeeProps, User } from "../types";

export const hashedPassword = (password: string): string => {
  const hashPassword = hashSync(password, 12);
  return hashPassword;
};

export const comparePassword = (
  password: string,
  hashedPassword: string
): boolean => {
  const comparePassword = compareSync(password, hashedPassword);
  return comparePassword;
};

export const signinToken = (user: User | EmployeeProps) => {
  const accessToken = sign(user, JWT_SECRET, { expiresIn: "1d" });
  return accessToken;
};
