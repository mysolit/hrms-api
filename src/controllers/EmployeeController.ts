import { NextFunction, Request, Response } from "express";
import { comparePassword, hashedPassword, signinToken } from "../helpers";
import { CreateError } from "../helpers/errors";
import prisma from "../lib/prisma";

export const Signin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const employee = await prisma.employee.findFirst({
      where: {
        email: req.body.email,
      },
      select: {
        id: true,
        password: true,
        employeeId: true,
        employee: true,
        email: true,
        phoneNumber: true,
        photo: true,
        dateOfBirth: true,
        position: true,
        joinDate: true,
        employeeType: true,
      },
    });
    if (!employee) return next(CreateError(404, "USER NOT FOUND !!"));
    const isPasswordCorrect = comparePassword(
      req.body.password,
      employee.password
    );
    if (!isPasswordCorrect)
      return next(CreateError(404, "PASSWORD INCORRECT."));
    const accessToken = signinToken({
      id: employee.id,
      employeeId: employee.employeeId,
      employee: employee.employee,
      email: employee.email,
      password: employee.password,
      phoneNumber: employee.phoneNumber,
      photo: employee.photo,
      dateOfBirth: employee.dateOfBirth,
      position: employee.position,
      joinDate: employee.joinDate,
      employeeType: employee.employeeType,
    });
    if (accessToken) {
      const { password, ...others } = employee;
      res
        .cookie("accessToken", accessToken, {
          httpOnly: true,
        })
        .status(200)
        .json({
          statusCode: 200,
          message: "success",
          ...others,
          accessToken,
        });
    }
  } catch (error) {
    next(error);
  }
};

export const CreateEmployeeInfo = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const emp = await prisma.employee.create({
      data: {
        employeeId: req.body.values.employeeId,
        employee: req.body.values.employee,
        email: req.body.values.email,
        password: hashedPassword(req.body.values.password),
        phoneNumber: req.body.values.phoneNumber,
        address: req.body.values.address,
        otherEmails: req.body.values.otherEmails,
        photo: req.body.values.photo,
        photoUrl: req.body.values.photoUrl,
        dateOfBirth: req.body.values.dateOfBirth,
        position: req.body.values.position,
        joinDate: req.body.values.joinDate,
        endDate: req.body.values.endDate,
        cv: req.body.values.cv,
        cvUrl: req.body.values.cvUrl,
        employeeType: req.body.values.employeeType,
        department: req.body.values.department,
        reportTo: req.body.values.reportTo,
        familyName: req.body.values.familyName,
        familyPhoneNumber: req.body.values.familyPhoneNumber,
        familyAddress: req.body.values.familyAddress,
        familyRelated: req.body.values.familyRelated,
        createdById: req.body.createdBy,
      },
    });
    if (emp) {
      res.status(200).json({
        statusCode: 200,
        message: `success`,
      });
    } else {
      res.status(422).json({
        statusCode: 422,
        message: `fail`,
      });
    }
  } catch (error: any) {
    next(error);
  }
};

export const EmployeeLists = async (
  _req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const employeeLists = await prisma.employee.findMany({
      include: {
        createdBy: {
          select: {
            id: true,
            employee: true,
            email: true,
            position: true,
          },
        },
      },
    });
    res.status(200).json({
      message: "success",
      statusCode: 200,
      data: employeeLists,
    });
  } catch (error) {
    next(error);
  }
};

export const SingleEmployee = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const employee = await prisma.employee.findUnique({
      where: {
        id: req.params.id,
      },
      include: {
        createdBy: {
          select: {
            id: true,
            employee: true,
            email: true,
            position: true,
            createdAt: true,
            updatedAt: true,
          },
        },
      },
    });
    res.status(200).json({
      message: "success",
      statusCode: 200,
      data: employee,
    });
  } catch (error) {
    next(error);
  }
};
