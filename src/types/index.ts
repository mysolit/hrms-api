export interface User {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  userTypes: string;
  updatedAt: string;
  createdAt: string;
}

export interface EmployeeProps {
  id: string;
  employeeId: string;
  employee: string;
  email: string;
  password: string;
  phoneNumber?: string | null | undefined;
  photo?: string | null | undefined;
  dateOfBirth?: string | null | undefined;
  position?: string | null | undefined;
  joinDate?: string | null | undefined;
  employeeType?: string | null | undefined;
}
